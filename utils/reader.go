/*
 Copyright 2016 Mike Lloyd

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package utils

import (
	"os"
	"bufio"
	"io"
	"time"
)

// FileReader takes an input file reads it line by line until the end of the file. If the end of the file is reached, it
// calls itself asynchronously after sleeping for 2 seconds, to repopulate the newer logs and start from where it left
// off.
func FileReader(input *os.File, start int64, log chan <- string) {
	if _, err := input.Seek(start, 0); err != nil {
		panic(err)
	}

	r := bufio.NewReader(input)
	pos := start
	for {
		data, err := r.ReadBytes('\n')
		pos += int64(len(data))
		if err == nil {
			if len(data) > 0 && data[len(data) - 1] == '\n' {
				data = data[:len(data) - 1]
			}
			if len(data) > 0 && data[len(data) - 1] == '\r' {
				data = data[:len(data) - 1]
			}
			log <- string(data)
		}
		// if EOF, wait 2 seconds to backfill, then call again.
		if err == io.EOF {
			time.Sleep(time.Second * 2)
			go FileReader(input, pos, log)
			break
		}
		if err != nil {
			if err != io.EOF {
				panic(err)
			}
			break
		}
	}
}