/*
 Copyright 2016 Mike Lloyd

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package utils

import (
	"time"
	"math/rand"
	"strings"
	"strconv"
	"github.com/satori/go.uuid"
	"fmt"
	"io/ioutil"
	"os"
	"sync"
)

type CommonLogFormat struct {
	HostIP         string
	UserIdent      string
	User           string
	Timestamp      string
	Request        string
	HttpStatusCode int
	Size           int
}

func BackfillAndStream(num int, paths []string, tps int, save bool, done <- chan bool) (*os.File) {
	tempLogFile, err := ioutil.TempFile(".", uuid.NewV4().String())
	if err != nil {
		panic(err)
	}

	GenerateRandomHistorical(num, paths, tempLogFile)
	go StreamCurrentRandomLogs(tempLogFile, paths, save, done)

	return tempLogFile
}

// GenerateRandom is a generator that generates random log messages so we can have test data. `num` is how many log
// historical lines you want to generate. Higher is better, but it's capped at 94608000, which would give one log
// line per second for the last relative 3 years.
func GenerateRandomHistorical(num int, paths []string, file *os.File) {

	var topWG sync.WaitGroup

	// we're going to batch write 10 lines at a time to the log file.
	logLines := make(chan *CommonLogFormat, 10)
	for i := 0; i < num; i++ {
		go RandomHistoricalLogGenerator(logLines, paths, &topWG)
	}

	go func() {
		topWG.Wait()
		close(logLines)
	}()

	// write them to disk, but don't close the file.
	for log := range logLines {
		file.WriteString(format(log))
	}

	return
}

// TODO test streaming logs.
// StreamCurrentRandomLogs streams logs to disk so it can be read by the console app.
func StreamCurrentRandomLogs(file *os.File, paths []string, save bool, done <- chan bool) {
	ticker := time.NewTicker(time.Second * 1)
	tpsQueue := make(chan *CommonLogFormat, 150)
	var streamGroup sync.WaitGroup
	go func() {
		for {
			select {
			case <-done:
				ticker.Stop()
				return
			case <-ticker.C:
				for i := 0; i < rand.Intn(150); i++ {
					go RandomStreamingLogGenerator(tpsQueue, paths, &streamGroup)
				}
			}
		}
	}()

	for log := range tpsQueue {
		file.WriteString(format(log))
	}

	streamGroup.Wait()
	close(tpsQueue)

	// if we're not saving the log file, close at the end of it. to get rid of the handler and delete the file.
	if !save {
		tName := file.Name()
		file.Close()
		err := os.Remove(tName)
		if err != nil {
			panic(err)
		}
	}

	return
}

func RandomHistoricalLogGenerator(ch chan <- *CommonLogFormat, paths []string, wg *sync.WaitGroup) {
	wg.Add(1)
	clf := &CommonLogFormat{}
	clf.randomIP()
	clf.randomRequest(paths)
	clf.UserIdent = "-"
	clf.randomUser()
	clf.Timestamp = time.Now().Format(time.RFC3339)
	clf.randomStatus()
	ch <- clf
	wg.Done()
}

func RandomStreamingLogGenerator(ch chan <- *CommonLogFormat, paths []string, wg *sync.WaitGroup) {
	wg.Add(1)
	clf := &CommonLogFormat{}
	clf.randomIP()
	clf.randomRequest(paths)
	clf.UserIdent = "-"
	clf.randomUser()
	clf.randomHistoricalTimestamp()
	clf.randomStatus()
	ch <- clf
	wg.Done()
}

// format returns the byte string of the log format so it's consistent with real-time CLF formatting. Also, because of
// stupid Windows vs POSIX-like systems, on Windows it will return CRLF. I develop on Windows, so platform independence
// is important to me. :)
func format(clf *CommonLogFormat) string {
	return fmt.Sprintf("%s %s %s [%s] \"%s\" %d %d\n",
		clf.HostIP,
		clf.UserIdent,
		clf.User,
		clf.Timestamp,
		clf.Request,
		clf.HttpStatusCode,
		clf.Size)
}

// randomIP generates a random IP address.
func (clf *CommonLogFormat) randomIP() {
	low, high := 1, 254
	randIP := make([]string, 0)

	for i := 0; i < 4; i++ {
		randIP = append(randIP, strconv.Itoa(rand.Intn(high - low) + low))
	}

	clf.HostIP = strings.Join(randIP, ".")
}

func (clf *CommonLogFormat) randomUser() {
	clf.User = uuid.NewV4().String()
}

func (clf *CommonLogFormat) randomHistoricalTimestamp() {
	// there are over 94 million seconds in the last 3 years, we'll seed the random number range.
	now := time.Now().Unix()
	threeYearsAgo := int64(now - 94608000)
	randomTime := rand.Int63n(now - threeYearsAgo) + threeYearsAgo

	randomNow := time.Unix(randomTime, 0)

	clf.Timestamp = randomNow.Format(time.RFC3339)
}

func (clf *CommonLogFormat) randomRequest(paths []string) {
	// some references.
	methods := []string{"GET", "HEAD"}
	files := map[string]int{
		"index.html": 2345,
		"script.js": 4567,
		"dynamic.js": 5678,
		"style.css": 6789,
		"some.jpg": 9876,
		"thing.png": 6345}

	// make it more or less random.
	randomMethod := rand.Intn(len(methods))
	randomPath := rand.Intn(len(paths))
	randClientVer := rand.Intn(2) + 1
	randFile := randomFile(files)

	clf.Request = fmt.Sprintf("%s /%s/%s HTTP/%d.0",
		methods[randomMethod],
		paths[randomPath],
		randFile,
		randClientVer)

	clf.Size = files[randFile]
}

func randomFile(m map[string]int) string {
	for k := range m {
		return k
	}
	return ""
}

func (clf *CommonLogFormat) randomStatus() {
	statuses := []int{200, 201, 202, 203, 204, 205, 206, 207, 208}
	randStatus := rand.Intn(len(statuses))
	clf.HttpStatusCode = statuses[randStatus]
}