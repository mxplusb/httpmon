/*
 Copyright 2016 Mike Lloyd

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package utils

import (
	"testing"
	"io/ioutil"
	"sync"
)

func TestGenerateRandomHistorical(t *testing.T) {
	paths := []string{"path1", "path2", "path3", "path4"}
	tmpFile, err := GenerateRandomHistorical(20, paths)
	if err != nil {
		t.Log(err)
		t.Fail()
	}
	defer tmpFile.Close()

	logs, err := ioutil.ReadFile(tmpFile.Name())
	if err != nil {
		t.Log(err)
		t.Fail()
	}
	t.Logf(string(logs))
}

func TestRandomLogGenerator(t *testing.T) {
	localLog := make(chan *CommonLogFormat, 10)
	paths := []string{"path1", "path2", "path3", "path4"}

	var wg sync.WaitGroup

	for i := 0; i < 150000; i++ {
		go RandomHistoricalLogGenerator(localLog, paths, &wg)
	}

	go func() {
		wg.Wait()
		close(localLog)
	}()

	// drain the channel
	for i := range localLog {
		_ = i
	}

}