/*
 Copyright 2016 Mike Lloyd

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package utils

import (
	"github.com/gizak/termui"
	"strings"
	"fmt"
	"sync"
	"time"
	"strconv"
)

type GlobalCounter struct {
	sync.RWMutex
	Counter        int
	Average        int
	CounterHistory []int
}

type PageStats struct {
	sync.RWMutex
	Gauge          *termui.Gauge
	AlertBox       *termui.List
	LogStreamBox   *termui.List
	PageCounter    int
	Alerts         []string
	Average        int
	AverageHistory []int
	AlertTime      time.Time
	PageName       string
}

func (g *GlobalCounter) Ticker(wg *sync.WaitGroup) {
	wg.Add(1)
	ticker := time.NewTicker(time.Minute * 2)
	for _ = range ticker.C {
		g.Lock()
		g.CounterHistory = append(g.CounterHistory, g.Counter)
		g.Average = g.CounterAverage()
		g.Counter = 0
		g.Unlock()
	}

}

func (g *GlobalCounter) CounterAverage() int {
	total := 0
	for _, i := range g.CounterHistory {
		total += i
	}
	return total / len(g.CounterHistory)
}

func (p *PageStats) average() int {
	total := 0
	for _, i := range p.AverageHistory {
		total += i
	}
	return total / len(p.AverageHistory)
}

func (p *PageStats) Reset() {
	p.Lock()
	p.PageCounter = 0
	p.Average = 0
	p.LogStreamBox.Items = []string{}
	p.AverageHistory = []int{}
	p.Unlock()
}

// Update updates the counter, and manages alarms.
func (p *PageStats) Update(g *GlobalCounter) {
	currentAverage := p.average()

	// do the time math to make sure it's been at least 60 seconds.
	delta := time.Now().Sub(p.AlertTime)

	if delta.Seconds() > 60 {
		if currentAverage > g.Average {
			p.alertString(false, currentAverage)
			p.AlertTime = time.Now()
		}
		if currentAverage < g.Average {
			p.alertString(true, currentAverage)
			p.AlertTime = time.Now()
		}
	}

	p.AverageHistory = append(p.AverageHistory, p.PageCounter)

	// lock the global counter so we can do atomic updates on the global counter.
	g.Lock()
	g.Counter += p.PageCounter
	//fmt.Printf("Updated global counter from %d to %d\n", g.Counter, g.Counter+ p.PageCounter)
	g.Unlock()

	percentile := int(percent(p.PageCounter, g.Counter))
	//fmt.Printf("%s is %0.4f%% of the global traffic.\n", p.PageName, percentile)

	p.Gauge.Percent = int(percentile)
	termui.Render(p.Gauge)
	termui.Render(p.AlertBox)
	termui.Render(p.LogStreamBox)

	return
}

func (p *PageStats) alertString(up bool, avg int) {
	s := ""
	if up {
		s = fmt.Sprintf("High traffic generated an alert - avg hits = %d, triggered at %s",
			avg,
			p.PageName)
	} else {
		s = fmt.Sprintf("traffic for %s has subsided to avg hits = %d.",
			p.PageName,
			avg)
	}

	p.Alerts = insertString(0, s, p.Alerts)
	p.AlertBox.Items = p.Alerts
}

func NewPageMetrics(page string) *PageStats {
	// build the page's relative traffic gauge.
	bc := termui.NewGauge()
	bc.BarColor = termui.ColorRed
	bc.Percent = 0
	bc.Height = 5
	bc.Label = "{{percent}}% Traffic"
	bc.BorderLabel = fmt.Sprintf("%s's traffic relative to global traffic", page)
	bc.LabelAlign = termui.AlignRight
	bc.PercentColorHighlighted = termui.ColorBlack
	bc.PercentColor = termui.ColorWhite

	// build the page's alerts.
	alertBox := termui.NewList()
	alertBox.ItemFgColor = termui.ColorRed
	alertBox.BorderLabel = fmt.Sprintf("%s's alerts", page)
	alertBox.Height = 30
	alertBox.Items = []string{}

	// build the page's streaming log box.
	logStream := termui.NewList()
	logStream.ItemFgColor = termui.ColorWhite
	logStream.BorderLabel = fmt.Sprintf("%s's streaming logs.", page)
	logStream.Items = []string{}
	logStream.Height = 20

	return &PageStats{
		PageCounter: 0,
		Alerts: make([]string, 1),
		AverageHistory: make([]int, 1),
		AlertTime: time.Now(),
		Average: 0,
		PageName: page,
		Gauge: bc,
		AlertBox: alertBox,
		LogStreamBox: logStream,
	}
}

func LogConversion(logs <- chan string, parsed chan <- *CommonLogFormat) {
	for message := range logs {
		clf := &CommonLogFormat{}
		messageStructure := strings.Fields(message)
		clf.HostIP = messageStructure[0]
		clf.UserIdent = messageStructure[1]
		clf.User = messageStructure[2]
		clf.Timestamp = messageStructure[3]
		clf.Request = strings.Join(messageStructure[4:7], " ")

		data, err := strconv.Atoi(messageStructure[7])
		if err != nil {
			panic(err)
		}
		clf.HttpStatusCode = data

		data, err = strconv.Atoi(messageStructure[8])
		if err != nil {
			panic(err)
		}
		clf.Size = data
		parsed <- clf
	}
	return
}

func percent(i, j int) float64 {
	return float64((float64(i) / float64(j)) * 400)
}

func insertString(idx int, val string, iter []string) []string {
	iter = append(iter[:idx],
		append([]string{val}, iter...)...)
	return iter
}


// CLFToPageStats take the CommonLogFormat structure and then makes useful metrics out of it for the pages.
func CLFToPageStats(logs <- chan *CommonLogFormat, pages ...*PageStats) {
	for log := range logs {
		// we need the path name so we can map it to a path, along with all the counter.
		pathName := func(s string) (string) {
			s1 := strings.Fields(s)
			s2 := strings.Split(s1[1], "/")
			return s2[1]
		}(log.Request)

		// grab the right page, then send 1
		for _, page := range pages {
			//fmt.Printf("parsed %s, looking for: %s\n", pathName, page.PageName)
			if pathName == page.PageName {
				page.Lock()
				//fmt.Printf("updating %s's counter from %d\n", page.PageName, page.PageCounter)
				page.PageCounter += 1
				page.LogStreamBox.Items = insertString(0, format(log), page.LogStreamBox.Items)
				page.Unlock()
			}
		}
	}
	return
}