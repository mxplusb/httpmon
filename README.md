# httpmon

This tool was designed to play around with console-/text-based dashboards for HTTP servers.

It does:

* Random log generation; no HTTP server needed.
* Stream randomly generated logs to a file in the same directory the binary is executed in to mimic an active HTTP server log.
* Reads from the same file independently of the logs being written.
* Performs a mediocre attempt at traffic percentile monitoring.
* Shows alerts based off relative percentile differences. 
* Streams the logs for each path on screen.

## how to use it

```shell
>.\httpmon.exe -h
Usage of .\httpmon.exe:
  -details
        Displays info.
  -nosave
        Removes the random logs. Upon program exit, the program will delete the log file location. Caution: the random log file is written to disk and will not be deleted afterwards.
  -num int
        Number of historical random log lines. Must be smaller than 94608000. (default 1500)
```

### license

Licensed under the Apache License v2.0